'''
Chap 3: Image processing

3.1.5 Application: Tonal adjustment
One of the most widely used applications of point-wise image processing operators is the
manipulation of contrast or tone in photographs, to make them look either more attractive or
more interpretable. You can get a good sense of the range of operations possible by opening
up any photo manipulation tool and trying out a variety of contrast,  brightness,  and color
manipulation options, as shown in Figures 3.2 and 3.7.
Exercises 3.1, 3.5,  and 3.6 have you implement some of these operations,  in order to
become familiar with basic image processing operators



'''
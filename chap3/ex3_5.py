"""
Ex 3.5: Photo effects
Write a variety of photo enhancement or effects filters:
contrast, solarization (quantization), etc.
Which ones are useful (perform sensible corrections) and
which ones are more creative (create unusual images)?

Effects:
<https://www.befunky.com/create/photo-effects/>
* chromatic
* black & white
* Charcoal
* Color pinhole
* Cooler
* Cross Process
* Cyanotype
* Grunge
* HDR
* Holga Art
* Instant
* Line Artopia
* Lomo Art
* Motion Color
* Old photo
*
"""

import cv2
import numpy as np
from matplotlib import pyplot as plt


def contrast(imgPath):
    """
    Use CLAHE (Contrast Limited Adaptive Histogram Equalization) to
    increase contrast of image

    Parameters
    ----------------
    imagePath: str
        Path to the image

    Returns
    ----------------------
    None
    """


    # -----Reading the image-----------------------------------------------------
    img = cv2.imread(imgPath, 1)


    # resize image
    # h, w = img.shape[:2]
    # newHeight = 600
    # newWidth = int(h / w * 600)
    # img = cv2.resize(img, (newWidth, newHeight))
    cv2.imshow("img", img)

    # -----Converting image to LAB Color model-----------------------------------
    lab = cv2.cvtColor(img, cv2.COLOR_BGR2LAB)
    cv2.imshow("lab", lab)

    # -----Splitting the LAB image to different channels-------------------------
    l, a, b = cv2.split(lab)
    cv2.imshow('l_channel', l)
    cv2.imshow('a_channel', a)
    cv2.imshow('b_channel', b)

    # -----Applying CLAHE to L-channel-------------------------------------------
    clahe = cv2.createCLAHE(clipLimit=3.0, tileGridSize=(8, 8))
    cl = clahe.apply(l)
    cv2.imshow('CLAHE output', cl)

    # -----Merge the CLAHE enhanced L-channel with the a and b channedl-----------
    limg = cv2.merge((cl, a, b))
    cv2.imshow('limg', limg)

    # -----Converting image from LAB Color model to RGB model--------------------
    final = cv2.cvtColor(limg, cv2.COLOR_LAB2BGR)
    cv2.imshow('final', final)

    # record key press
    k = cv2.waitKey(0) & 0xFF
    cv2.destroyAllWindows()

def main():
    contrast('../img/river1.jpg')


main()





















"""
Ex 3.1: Color balance
Write a simple application to change the color balance of an image
by multiplying each color value by a different user-specified constant.
If you want to get fancy, you can make this application interactive, with sliders.

1. Do you get different results if you take out the gamma transformation before or after
doing the multiplication? Why or why not?
=> Yes. because gamma correction also make image brighter, darker


"""

import cv2
from matplotlib import pyplot as plt
import numpy as np

def simpleMultiply(imgPath, value):
    """
    value: 0 < value < 1: image darker
    value: 1 < value: image brighter and maybe destroyed
    @param imgPath: image path
    @param value: constant multiply value
    @return: processed image
    """

    # load image
    img = cv2.imread(imgPath, -1)
    imgHeight, imgWidth = img.shape[:2]


    # multiply
    for y in range(0, imgHeight):
        for x in range(0, imgWidth):
            img[y, x] = img[y, x] * value # access row, col

    img = gammaCorrection(img)

    # show image
    result = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)  # because matplotlib use RGB
    plt.imshow(result)
    plt.axis("off")
    plt.show()


def gammaCorrection(img, gamma=2.2):

    # get image property
    invGamma = 1/gamma

    # lookup table
    # so image can map 1 => 20, 2=>28, ...
    table = np.array([ ((i / 255.0) ** invGamma) * 255
                      for i in np.arange(0, 256)]).astype("uint8")

    # apply gamma correction using lookup table
    return cv2.LUT(img, table)

def main():
    simpleMultiply("../img/Tu1.jpg", 0.5)

main()

